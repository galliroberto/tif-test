<?php

namespace Tests\TIF\Application\Service;

use DateTimeImmutable;
use TIF\Application\Service\ConvertitoreDiValute;
use TIF\Application\Service\OttieniTransazioniPerCliente;
use PHPUnit\Framework\TestCase;
use TIF\Domain\Model\Transazione;
use TIF\Domain\Service\ConvertiTransazioneInEuro;
use TIF\Domain\ValueObject\Valore;
use TIF\Infrastructure\Domain\InMemoryTransazioni;

class OttieniTransazioniPerClienteTest extends TestCase
{
    /**
     * @test
     * @dataProvider transazioni_provider
     */
    public function ottieni_transazioni_per_cliente(array $transazioniProvider): void
    {
        $transazioni = new InMemoryTransazioni();
        foreach ($transazioniProvider as $transazione) {
            $transazioni->add($transazione);
        }

        $this->assertCount(3, $transazioni->findByClienteId(1));
        $this->assertCount(3, $transazioni->findByClienteId(2));
        $this->assertCount(0, $transazioni->findByClienteId(3));
    }

    public function transazioni_provider(): array
    {
        return [
            [
                [
                    Transazione::crea(1, new DateTimeImmutable(), Valore::crea('$15.25')),
                    Transazione::crea(1, new DateTimeImmutable(), Valore::crea('$25.25')),
                    Transazione::crea(1, new DateTimeImmutable(), Valore::crea('$15.25')),
                    Transazione::crea(2, new DateTimeImmutable(), Valore::crea('£15.25')),
                    Transazione::crea(2, new DateTimeImmutable(), Valore::crea('£25.25')),
                    Transazione::crea(2, new DateTimeImmutable(), Valore::crea('£35.25')),
                ],
            ],
        ];
    }
}
