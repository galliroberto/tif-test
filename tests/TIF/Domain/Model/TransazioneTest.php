<?php

namespace Tests\TIF\Domain\Model;

use TIF\Domain\Model\Transazione;
use PHPUnit\Framework\TestCase;
use TIF\Domain\ValueObject\Valore;

class TransazioneTest extends TestCase
{
    /**
     * @test
     * @dataProvider parametri_validi_provider
     */
    public function crea_transazione_con_parametri_validi(int $clienteId, string $data, string $valore): void
    {
        $transazione = Transazione::crea(
            $clienteId,
            \DateTimeImmutable::createFromFormat('d/m/Y', $data),
            Valore::crea($valore)
        );
        $this->assertEquals($clienteId, $transazione->clienteId());
        $this->assertEquals($data, $transazione->data()->format('d/m/Y'));
        $this->assertEquals($valore, $transazione->valore()->valore());
    }

    public function parametri_validi_provider(): array
    {
        return [
            [
                1,
                '25/12/1999',
                '€15.50'
            ],
            [
                2,
                '27/12/1999',
                '$5435.50'
            ],
            [
                3,
                '15/12/2199',
                '£15.50'
            ],
        ];
    }
}
