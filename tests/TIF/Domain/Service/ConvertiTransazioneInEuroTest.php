<?php

namespace Tests\TIF\Domain\Service;

use DateTimeImmutable;
use Mockery;
use TIF\Application\Service\ConvertitoreDiValute;
use TIF\Domain\Model\Transazione;
use TIF\Domain\Service\ConvertiTransazioneInEuro;
use PHPUnit\Framework\TestCase;
use TIF\Domain\ValueObject\Valore;

class ConvertiTransazioneInEuroTest extends TestCase
{
    /**
     * @test
     * @dataProvider transazioni_provider
     */
    public function converti_transazione_in_euro(Transazione $transazioneProvider): void
    {
        $fattoreDiConversione = 1.25;
        $quantitaInEuro = number_format($fattoreDiConversione * $transazioneProvider->valore()->quantita(), 2);

        $convertitoreMock = Mockery::mock(ConvertitoreDiValute::class);
        $convertitoreMock
            ->shouldReceive('ottieniConversioneValutaInEuroDaValuta')
            ->andReturn($quantitaInEuro)
        ;

        $convertitore = new ConvertiTransazioneInEuro($convertitoreMock);

        $transazioneInEuro = $convertitore->execute($transazioneProvider);
        $this->assertEquals($quantitaInEuro, $transazioneInEuro->valoreInEuro()->quantita());
    }

    public function transazioni_provider(): array
    {
        return [
            [Transazione::crea(1, new DateTimeImmutable(), Valore::crea('$15.25'))],
            [Transazione::crea(1, new DateTimeImmutable(), Valore::crea('$25.25'))],
            [Transazione::crea(1, new DateTimeImmutable(), Valore::crea('$15.25'))],
            [Transazione::crea(2, new DateTimeImmutable(), Valore::crea('£15.25'))],
            [Transazione::crea(2, new DateTimeImmutable(), Valore::crea('£25.25'))],
            [Transazione::crea(2, new DateTimeImmutable(), Valore::crea('£35.25'))],
        ];
    }
}
