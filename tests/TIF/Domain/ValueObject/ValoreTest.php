<?php

namespace Tests\TIF\Domain\ValueObject;

use InvalidArgumentException;
use TIF\Domain\ValueObject\Valore;
use PHPUnit\Framework\TestCase;

class ValoreTest extends TestCase
{
    /**
     * @test
     * @dataProvider parametri_validi_provider
     */
    public function crea_valore_con_parametri_validi(string $valoreProvider): void
    {
        $this->assertEquals($valoreProvider, Valore::crea($valoreProvider)->valore());
    }

    public function parametri_validi_provider(): array
    {
        return [
            ['€15.50'],
            ['$25.50'],
            ['£35.50'],
            ['€15.51'],
            ['$25.52'],
            ['£35.53'],
        ];
    }

    /**
     * @test
     * @dataProvider parametri_non_validi_provider
     */
    public function crea_valore_con_parametri_non_validi(string $valoreProvider): void
    {
        $this->expectException(InvalidArgumentException::class);

        Valore::crea($valoreProvider);
    }

    public function parametri_non_validi_provider(): array
    {
        return [
            ['&15'],
            ['?25'],
            ['£35.qualcosa'],
            ['&15.5'],
            ['?25.5'],
            ['€test'],
            ['$test'],
        ];
    }
}
