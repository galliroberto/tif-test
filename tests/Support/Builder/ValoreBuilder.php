<?php

namespace Tests\Support\Builder;

use TIF\Domain\ValueObject\Sesso;
use TIF\Domain\ValueObject\Valore;

class ValoreBuilder
{
    private string $valore;

    protected function __construct()
    {
        $this->valore = '$15.25';
    }

    public function withValore(string $valore): self
    {
        $this->valore = $valore;
        return $this;
    }

    public static function crea(): self
    {
        return new static();
    }

    public function build(): Valore
    {
        return Valore::crea($this->valore);
    }
}