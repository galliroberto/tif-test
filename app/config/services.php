<?php

declare(strict_types=1);

use TIF\Application\Presenter\MostraLeTransazioniInConsole;
use TIF\Application\Service\OttieniTransazioniPerCliente;
use TIF\Application\Service\ConvertiTransazioniInEuro;
use TIF\Domain\Service\ConvertiTransazioneInEuro;
use TIF\Infrastructure\Domain\InMemoryTransazioni;
use TIF\Infrastructure\Domain\PhalconTransazioni;
use TIF\Infrastructure\Application\Service\ConvertitoreDiValuteXYZ;

$container->set(
    'ottieni-transazioni-per-cliente',
    [
        'className' => OttieniTransazioniPerCliente::class,
        'arguments' => [
            [
                'type' => 'parameter',
                'name' => 'inMemoryTransazioni',
                'value' => new PhalconTransazioni(),
            ],
        ]
    ]
);

$container->set(
    'converti-transazioni-in-euro',
    [
        'className' => ConvertiTransazioniInEuro::class,
        'arguments' => [
            [
                'type' => 'parameter',
                'name' => 'convertiTransazioneInEuro',
                'value' => new ConvertiTransazioneInEuro(new ConvertitoreDiValuteXYZ()),
            ],
        ]
    ]
);


$container->set(
    'mostra-le-transazioni-in-console',
    [
        'className' => MostraLeTransazioniInConsole::class,
    ]
);

