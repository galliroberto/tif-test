README
==

## per usare docker
per prima cosa andare nella cartella docker e copare il file docker-compose.override.yaml.dist in docker-compose.override.yaml
al suo interno sono mappate le porte per i servizi
i paramentri di connessione possono non essere sovrascritti e lasciati quelli di default

### docker
per lanciare la 
```./dc up```

oppure detached
```./dc up -d```

se detached per fermare la docker
```./dc stop```

### per entrare nel container
```./dc enter```

### per installare i vendor
da dentro il container
```composer install```

### tests
da dentro il container
```vendor/bin/phpunit```

### esecuzione programma
da dentro il container

```php cli.php MostraTransazioniPerCliente run cliente_id```

clienti disponibili ```[1, 2, 3]```

i dati delle transazioni sono presenti in 
```src/TIF/Infrastructure/Domain/PhalconTransazioni.php```

la configurazione della dependency injection si trova in 
```app/config/services.php```

---

DEV CHALLENGE
===============================

### Obiettivo

Dimostrare le tue competenze di programmazione ad oggetti e di preparazione
test automatici.

### Task

Creare un semplice report che mostri le transazioni per un customer id
specificato come argomento da linea di comando.

Il file data.csv contiene dati di esempio in varie valute, il report dovrà
mostrare i valori in EUR.

Assumi che la base dati sia dinamica e che i dati arrivino da un database, il
csv è usato solo per semplicità di implementazione.

Aggiungi il codice, i test e la documentazione che ritieni opportuna
(dockblock, commenti, README).  Non è necessario che l'applicazione utilizzi un
webservice di cambio valuta reale, un client finto che ritorna valori random o
fissi è sufficiente.

Il codice fornito è solo un'indicazione di massima, non è necessario
utilizzarlo se non lo trovi utile. Se qualche requisito non ti è chiaro,
sentiti libero di improvvisare.

Puoi utilizzare framework o librerie già pronte se lo ritieni opportuno. Se
utilizzi codice di terze parti ti consigliamo di utilizzare composer.

### Valutazione

Il task sarà valutato sulla base del tuo utilizzo della programmazione OO,
della dependency injection (ove opportuno) e della leggibilità e manutenibilità.
In tal senso, la copertura con test automatici è fortemente incoraggiata.

Ti invitiamo a curare tutti gli aspetti come se si trattasse di un progetto
pronto per andare in produzione. In tal senso, ad esempio, non consegnare file
o codice inutilizzato, soprattutto se proveniente da framework di terze parti. 


### Alternativa linguaggio (PHP o Javascript)
In alternativa, se ti trovi più a tuo agio in ambiente javascript/nodejs puoi 
realizzare una semplice API che restituisca il report richiesto tramite chiamata 
REST/HTTP. 

Anche (e soprattutto) in questo caso è indispensabile indicare nel README tutti i 
passaggi necessari all'installazione e verifica.