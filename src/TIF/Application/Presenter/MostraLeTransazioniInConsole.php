<?php

namespace TIF\Application\Presenter;

use LucidFrame\Console\ConsoleTable;

class MostraLeTransazioniInConsole
{
    public function execute(array $transazioni): void {
        $table = new ConsoleTable();
        $table
            ->addHeader('Cliente')
            ->addHeader('Data')
            ->addHeader('Valore')
            ->addHeader('Valore €');

        foreach ($transazioni as $transazione) {
            $table->addRow()
                ->addColumn($transazione->clienteId())
                ->addColumn($transazione->data()->format('d-m-Y'))
                ->addColumn($transazione->valore())
                ->addColumn($transazione->valoreInEuro()
                );
        }

        $table->display();
    }
}