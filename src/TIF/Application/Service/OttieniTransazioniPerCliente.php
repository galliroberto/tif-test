<?php

namespace TIF\Application\Service;

use TIF\Domain\Model\Transazioni;

class OttieniTransazioniPerCliente
{
    private Transazioni $transazioni;

    public function __construct(Transazioni $transazioni)
    {
        $this->transazioni = $transazioni;
    }

    public function execute(int $clienteId): array
    {
        return $this->transazioni->findByClienteId($clienteId);
    }
}