<?php

namespace TIF\Application\Service;

interface ConvertitoreDiValute
{
    public function ottieniConversioneValutaInEuroDaValuta(string $valuta, float $quantita): float;
}