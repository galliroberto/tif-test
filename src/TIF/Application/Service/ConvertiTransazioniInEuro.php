<?php

namespace TIF\Application\Service;

use TIF\Domain\Model\Transazione;
use TIF\Domain\Service\ConvertiTransazioneInEuro;

class ConvertiTransazioniInEuro
{
    private ConvertiTransazioneInEuro $convertiTransazioneInEuro;

    public function __construct(ConvertiTransazioneInEuro $convertiTransazioneInEuro)
    {
        $this->convertiTransazioneInEuro = $convertiTransazioneInEuro;
    }

    public function execute(array $transazioni): array
    {
        return array_map(function (Transazione $transazione) {
            return $this->convertiTransazioneInEuro->execute($transazione);
        },
            $transazioni
        );
    }

}