<?php

namespace TIF\Infrastructure\Domain;


use DateTimeImmutable;
use TIF\Domain\Model\Transazione;
use TIF\Domain\Model\Transazioni;
use TIF\Domain\ValueObject\Valore;

class PhalconTransazioni implements Transazioni
{
    private $transazioni = [];

    public function __construct()
    {
        /*
         * Inizializzo il "DB" con valori
         */
        $this->transazioni = [
            Transazione::crea(1, new DateTimeImmutable(), Valore::crea('$15.25')),
            Transazione::crea(1, new DateTimeImmutable(), Valore::crea('$25.25')),
            Transazione::crea(1, new DateTimeImmutable(), Valore::crea('$15.25')),
            Transazione::crea(2, new DateTimeImmutable(), Valore::crea('£15.25')),
            Transazione::crea(2, new DateTimeImmutable(), Valore::crea('£25.25')),
            Transazione::crea(2, new DateTimeImmutable(), Valore::crea('£35.25')),
            Transazione::crea(3, new DateTimeImmutable(), Valore::crea('£15.25')),
            Transazione::crea(3, new DateTimeImmutable(), Valore::crea('£25.25')),
            Transazione::crea(3, new DateTimeImmutable(), Valore::crea('£35.25')),
        ];
    }

    public function findByClienteId(int $clienteId): array
    {
        return array_filter($this->transazioni, function (Transazione $transazione) use ($clienteId) {
            return $clienteId === $transazione->clienteId();
        });
    }

    public function deleteAll(): void
    {
        $this->transazioni = [];
    }

    public function add(Transazione $transazione): void
    {
        $this->transazioni[] = $transazione;
    }
}

