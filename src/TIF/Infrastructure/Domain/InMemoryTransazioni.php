<?php

namespace TIF\Infrastructure\Domain;

use DateTimeImmutable;
use TIF\Domain\Model\Transazione;
use TIF\Domain\Model\Transazioni;
use TIF\Domain\ValueObject\Valore;

class InMemoryTransazioni implements Transazioni
{
    private $transazioni = [];

    public function findByClienteId(int $clienteId): array
    {
        return array_filter($this->transazioni, function (Transazione $transazione) use ($clienteId) {
            return $clienteId === $transazione->clienteId();
        });
    }

    public function deleteAll(): void
    {
        $this->transazioni = [];
    }

    public function add(Transazione $transazione): void
    {
        $this->transazioni[] = $transazione;
    }
}
