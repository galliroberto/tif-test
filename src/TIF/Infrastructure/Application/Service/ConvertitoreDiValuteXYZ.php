<?php

namespace TIF\Infrastructure\Application\Service;

use InvalidArgumentException;
use TIF\Application\Service\ConvertitoreDiValute;
use TIF\Domain\ValueObject\Valore;

class ConvertitoreDiValuteXYZ implements ConvertitoreDiValute
{
    private array $conversioni = [
        Valore::GBP => 0.750,
        Valore::USD => 0.95,
        Valore::EUR => 1,
    ];

    public function ottieniConversioneValutaInEuroDaValuta(string $valuta, float $quantita): float
    {
        if(!isset($this->conversioni[$valuta])) {
            throw new InvalidArgumentException(sprintf('Valuta non valida [%s]', $valuta));
        }

        return floatval(number_format($this->conversioni[$valuta] * $quantita, 2));
    }
}