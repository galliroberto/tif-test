<?php

declare(strict_types=1);

namespace TIF\Infrastructure\Communication\Command;

use Faker\Factory;
use Faker\Provider\it_IT\Address;
use LucidFrame\Console\ConsoleTable;
use Phalcon\Cli\Task;
use TIF\Application\Presenter\MostraLeTransazioniInConsole;
use TIF\Application\Service\OttieniTransazioniPerCliente;
use TIF\Application\Service\ConvertiTransazioniInEuro;

class TransazioniPerClienteCommand extends Task
{
    private OttieniTransazioniPerCliente $ottieniTransazioniPerCliente;
    private ConvertiTransazioniInEuro $convertiTransazioniInEuro;
    private MostraLeTransazioniInConsole $mostraLeTransazioniInConsole;

    public function onConstruct()
    {
        //print_r($this->getDi()); die;
        $this->ottieniTransazioniPerCliente = $this->getDi()->get('ottieni-transazioni-per-cliente');
        $this->convertiTransazioniInEuro = $this->getDi()->get('converti-transazioni-in-euro');
        $this->mostraLeTransazioniInConsole = $this->getDi()->get('mostra-le-transazioni-in-console');
    }

    public function mainAction()
    {
        echo 'Per lanciare lo script esegui php cli.php TransazioniPerCliente run [cliente_id]' . PHP_EOL;
    }

    public function runAction(int $clienteId)
    {
        $transazioni = $this->ottieniTransazioniPerCliente->execute($clienteId);
        $transazioniInEuro = $this->convertiTransazioniInEuro->execute($transazioni);
        $this->mostraLeTransazioniInConsole->execute($transazioniInEuro);
    }
}