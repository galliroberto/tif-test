<?php

declare(strict_types=1);

namespace TIF\Domain\ValueObject;

use InvalidArgumentException;

final class Valore
{
    const EUR = '€';
    const USD = '$';
    const GBP = '£';

    private array $valuteAmmesse = [
        self::EUR,
        self::USD,
        self::GBP,
    ];

    private string $valore;
    private string $valuta;
    private string $quantita;

    private function __construct(string $valore)
    {
        $valore = trim(mb_substr($valore, 0));

        $this->valuta = $this->estraiValutaDaValore($valore);
        $this->quantita = $this->estraiQuantitaDaValore($valore);
        $this->valore = $this->valuta . $this->quantita;
    }

    public static function crea(string $valore): self
    {
        return new self($valore);
    }

    private function estraiValutaDaValore(string $valore): string
    {
        $valuta = mb_substr($valore, 0, 1);
        $this->validaValutaOrFail($valuta);
        return $valuta;
    }

    private function estraiQuantitaDaValore(string $valore): string
    {
        $quantita = mb_substr($valore, 1);
        $this->validaQuantitaOrFail($quantita);
        return number_format(floatval($quantita), 2, '.', '');
    }

    private function validaValutaOrFail(string $valuta): void
    {
        if (!in_array($valuta, $this->valuteAmmesse)) {
            throw new InvalidArgumentException(sprintf('Valuta non valida [%s]', $valuta));
        }
    }

    private function validaQuantitaOrFail(string $quantita): void
    {
        if (!is_numeric($quantita)) {
            throw new InvalidArgumentException(sprintf('Quantita non valida [%s]', $quantita));
        }
    }

    public function valore(): string
    {
        return $this->valore;
    }

    public function valuta(): string
    {
        return $this->valuta;
    }

    public function quantita(): string
    {
        return $this->quantita;
    }

    public function toArray(): array
    {
        return [
            'valore' => $this->valore,
            'valuta' => $this->valuta,
            'quantita' => $this->quantita,
        ];
    }

    public function __toString(): string
    {
        return $this->valore;
    }

    public function __toClone(): void
    {
    }
}