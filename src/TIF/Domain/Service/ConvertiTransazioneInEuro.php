<?php

namespace TIF\Domain\Service;

use TIF\Application\Service\ConvertitoreDiValute;
use TIF\Domain\Model\Transazione;
use TIF\Domain\ValueObject\Valore;

class ConvertiTransazioneInEuro
{
    private ConvertitoreDiValute $convertitoreDiValute;

    public function __construct(ConvertitoreDiValute $convertitoreDiValute)
    {
        $this->convertitoreDiValute = $convertitoreDiValute;
    }

    public function execute(Transazione $transazione): Transazione
    {
        $quantitaInEuro = $this->convertitoreDiValute->ottieniConversioneValutaInEuroDaValuta(
            $transazione->valore()->valuta(),
            $transazione->valore()->quantita()
        );
        $valoreInEuro = Valore::crea(sprintf('%s%s', Valore::EUR, $quantitaInEuro));
        $transazione->impostaValoreInEuro($valoreInEuro);

        return $transazione;
    }
}