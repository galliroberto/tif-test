<?php

namespace TIF\Domain\Model;

interface Transazioni
{
    public function findByClienteId(int $clienteId): array;
    public function deleteAll(): void;
    public function add(Transazione $transazione): void;
}
