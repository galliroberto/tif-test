<?php

namespace TIF\Domain\Model;

use DateTimeImmutable;
use TIF\Domain\Aggregate\Misura\Misure;
use TIF\Domain\ValueObject\MisurazioneParametri;
use TIF\Domain\ValueObject\ClienteId;
use TIF\Domain\ValueObject\Valore;

final class Transazione
{
    private int $clienteId;
    private DateTimeImmutable $data;
    private Valore $valore;
    private Valore $valoreInEuro;

    private function __construct(
        int $clienteId,
        DateTimeImmutable $data,
        Valore $valore
    ) {
        $this->clienteId = $clienteId;
        $this->data = $data;
        $this->valore = $valore;
    }

    public static function crea(
        int $clienteId,
        DateTimeImmutable $data,
        Valore $valore
    ): self {
        return new self($clienteId, $data, $valore);
    }

    public function impostaValoreInEuro(Valore $valoreInEuro): void
    {
        $this->valoreInEuro = $valoreInEuro;
    }

    public function clienteId(): int
    {
        return $this->clienteId;
    }

    public function data(): DateTimeImmutable
    {
        return $this->data;
    }

    public function valore(): Valore
    {
        return $this->valore;
    }

    public function valoreInEuro(): Valore
    {
        return $this->valoreInEuro;
    }
}