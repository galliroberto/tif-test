<?php

namespace Common\DTO;

class ResponseDto
{
    public const STATUS_SUCCESS = 'success';
    public const STATUS_FAIL = 'fail';
    private string $status;
    private int $code;
    private string $message;
    private array $errors;

    private function __construct(string $status, int $code, string $message)
    {
        $this->status = $status;
        $this->code = $code;
        $this->message = $message;
        $this->errors = [];
    }

    public static function success(string $message): self
    {
        return new static(self::STATUS_SUCCESS, 0, $message);
    }

    public static function fail(string $message): self
    {
        return new static(self::STATUS_FAIL, 1, $message);
    }

    public function code(): int
    {
        return $this->code;
    }

    public function message(): string
    {
        return $this->message;
    }

    public function errors(): array
    {
        return $this->errors;
    }

    public function withErrors(array $errors): self
    {
        $this->errors = $errors;

        return $this;
    }

    public function isSuccess(): bool
    {
        return self::STATUS_SUCCESS === $this->status;
    }

    public function isFail(): bool
    {
        return self::STATUS_FAIL === $this->status;
    }
}